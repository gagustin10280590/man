package principal;


/**
 *
 * @author beto
 */
import javax.persistence.*;
import entidad.*;
import java.util.Date;
public class Principal 
{
    public static void main(String args[])
    {
        EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("Linea-OrdenPU");
        EntityManager em = emfactory.createEntityManager();
        em.getTransaction().begin();
        Date fecha = new Date();
        
        Producto producto = new Producto();
        producto.setIdProducto(1);
        producto.setDescripcion("producto 1");
        producto.setPrecio(88.9f);
        
        Producto producto1 = new Producto();
        producto1.setIdProducto(2);
        producto1.setDescripcion("producto2");
        producto1.setPrecio(40.89f);
        
        Producto producto2 = new Producto();
        producto2.setIdProducto(3);
        producto2.setDescripcion("producto3");
        producto2.setPrecio(45.09f);
       
        
        Orden ord= new Orden();
        ord.setIdOrden(1);
        ord.setFechaOrden(fecha);
        
        Orden ord1= new Orden();
        ord1.setIdOrden(2);
        ord1.setFechaOrden(fecha);
        
        Orden ord2= new Orden();
        ord2.setIdOrden(3);
        ord2.setFechaOrden(fecha);
        
        LineaOrden lineOr= new LineaOrden(1,1);
        lineOr.setCantidad(4);       
        
        LineaOrden lineOr1= new LineaOrden(1,2);
        lineOr1.setCantidad(2);
        
        LineaOrden lineOr2= new LineaOrden(2,1);
        lineOr2.setCantidad(1);
        
        LineaOrden lineOr3= new LineaOrden(2,2);
        lineOr3.setCantidad(6);
        
        try {
            em.persist(producto);
            em.persist(producto1);
            em.persist(producto2);
            
            em.persist(ord);
            em.persist(ord1);
            em.persist(ord2);
            
            em.persist(lineOr);
            em.persist(lineOr1);
            em.persist(lineOr2);
            em.persist(lineOr3);
           
            
        } catch (Exception e) {
            System.out.println("Error al regustrar los datos!!!" + e.getMessage()); 

        }
        
        System.out.println("Imprimiendo lista de datos");
        int p1Id = producto.getIdProducto();
        int p2Id = producto1.getIdProducto();
        int p3Id = producto2.getIdProducto();
        
        Producto pructoDatos = em.find(Producto.class, p1Id);
        System.out.println("Imprimiendo datos del Porducto 1:");
        System.out.println("Id: " + pructoDatos.getIdProducto());
        System.out.println("Descripción: " + pructoDatos.getDescripcion());
        System.out.println("Precio: " + pructoDatos.getPrecio());
        
        Producto pructoDatos1 = em.find(Producto.class, p2Id);
        System.out.println("Imprimiendo datos del Porducto 2:");
        System.out.println("Id: " + pructoDatos1.getIdProducto());
        System.out.println("Descripción: " + pructoDatos1.getDescripcion());
        System.out.println("Precio: " + pructoDatos1.getPrecio());
        
        Producto pructoDatos2 = em.find(Producto.class, p3Id);
        System.out.println("Imprimiendo datos del Porducto 3:");
        System.out.println("Id: " + pructoDatos2.getIdProducto());
        System.out.println("Descripción: " + pructoDatos2.getDescripcion());
        System.out.println("Precio: " + pructoDatos2.getPrecio());
        
        System.out.println("Imprimiendo datos dela orden");
        
        int idOrden = ord1.getIdOrden();
        int idOrden1 = ord2.getIdOrden();

        Orden ordenBase = em.find(Orden.class, idOrden);
        System.out.println("Imprimiendo datos de la Orden 1");
        System.out.println("Id: " + ordenBase.getIdOrden());
        System.out.println("Fecha: " + ordenBase.getFechaOrden());
        
        Orden ordenBase1 = em.find(Orden.class, idOrden);
        System.out.println("Imprimiedno  datos de la Orden 2");
        System.out.println("Id: " + ordenBase1.getIdOrden());
        System.out.println("Fecha: " + ordenBase1.getFechaOrden());
        


        System.out.println("Datos de linea de Orden");


        LineaOrdenPK linea= lineOr.getLineaOrdenPK();
        LineaOrden linea2=em.find(LineaOrden.class, linea);

        System.out.println("Imprimiendo datos de la Linea De Orden 1");
        System.out.println(linea2.getLineaOrdenPK());        
        System.out.println(linea2.getOrden());
        System.out.println(linea2.getProducto());
        System.out.println(linea2.getCantidad());
        
        LineaOrdenPK linea3= lineOr2.getLineaOrdenPK();
        LineaOrden linea4=em.find(LineaOrden.class, linea3);

        System.out.println("Imprimiendo datos de la Linea De Orden 2");
        System.out.println(linea4.getLineaOrdenPK());        
        System.out.println(linea4.getOrden());
        System.out.println(linea4.getProducto());
        System.out.println(linea4.getCantidad());
        
        LineaOrdenPK linea5= lineOr3.getLineaOrdenPK();

        LineaOrden linea6=em.find(LineaOrden.class, linea5);

        System.out.println("Imprimiedno datos de la Linea De Orden 3");
        System.out.println(linea6.getLineaOrdenPK());        
        System.out.println(linea6.getOrden());
        System.out.println(linea6.getProducto());
        System.out.println(linea6.getCantidad());
        
        
        try {
            em.getTransaction().commit();
        } catch (Exception e) {
            System.out.println("ERROR"+ e.getMessage());

        }

        em.close();
        emfactory.close();
        
       
        
    }
    
}
